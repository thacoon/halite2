#!/usr/bin/env bash

set -e

cmake .
make MyBot
make DefaultBot
./halite -d "240 160" "./MyBot" "./DefaultBot"
