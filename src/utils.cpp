#include "utils.hpp"

namespace src {
    const hlt::Planet &get_nearest_planet(const hlt::Ship &ship, std::vector<hlt::Planet> planets) {
        int nearest_planet_idx = 0;
        double min_distance = ship.location.get_distance_to(planets.at(0).location);

        for(size_t i=0; i<planets.size(); ++i) {
            const hlt::Location& target = ship.location.get_closest_point(planets.at(i).location, planets.at(i).radius);
            double distance = ship.location.get_distance_to(target);

            if(planets.at(i).owned) {
                continue;
            }

            if(distance < min_distance) {
                min_distance = distance;
                nearest_planet_idx = i;
            }
        }

        return planets.at(nearest_planet_idx);
    }
}

