#ifndef PROJECT_UTILS_H
#define PROJECT_UTILS_H

#include "hlt/hlt.hpp"
#include "hlt/location.hpp"

namespace src {
    const hlt::Planet &get_nearest_planet(const hlt::Ship &ship, std::vector<hlt::Planet> planets);
}

#endif //PROJECT_UTILS_H
