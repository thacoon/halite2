#include <vector>

#include <gmock/gmock.h>

#include "hlt/planet.hpp"
#include "hlt/ship.hpp"

#include "src/utils.hpp"

using namespace ::testing;


class Utils : public Test {
public:
    hlt::Ship ship;

    hlt::Planet nearest_planet;
    hlt::Planet further_planet;

    std::vector<hlt::Planet> planets;

    void SetUp() override {
        ship.location.pos_x = 0;
        ship.location.pos_y = 0;

        nearest_planet.owned = false;
        nearest_planet.radius = 1;
        nearest_planet.location.pos_x = 2;
        nearest_planet.location.pos_y = 1;
        planets.push_back(nearest_planet);

        further_planet.owned = false;
        further_planet.radius = 1;
        further_planet.location.pos_x = 5;
        further_planet.location.pos_y = 3;
        planets.push_back(further_planet);
    }
};


TEST_F(Utils, get_nearest_planet) {
    const hlt::Planet &actual_planet = src::get_nearest_planet(ship, planets);

    ASSERT_THAT(actual_planet.location.pos_x, Eq(nearest_planet.location.pos_x));
    ASSERT_THAT(actual_planet.location.pos_y, Eq(nearest_planet.location.pos_y));
}
